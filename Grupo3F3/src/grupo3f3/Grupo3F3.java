package grupo3f3;
import java.util.Scanner;

public class Grupo3F3 {

    public static void main(String[] args) {
        int n=0;
        double altura=0, lado1=0, lado2=0;
        
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Introduza as dimensões dos dois lados da base.");
        lado1= scanner.nextInt();
        lado2= scanner.nextInt();
        System.out.println("Introduza a altura.");
        altura= scanner.nextInt();
        
        while(n == 0){
            System.out.println("Calcular a área das bases  - 1" + "\n"
                    + "Calcular a área lateral - 2" + "\n"
                    + "Calcular a área total - 3" + "\n"
                    + "Calcular o volume - 4" + "\n"
                    + "Sair - 5"
                    );
            n = scanner.nextInt();
            switch(n){
                case 1: System.out.println("A área das bases é: ");System.out.println(Abases(altura, lado1, lado2));;
                n = 0;
                break;
                case 2: System.out.println("A área lateral é: ");System.out.println(Alateral(altura, lado1, lado2));
                n = 0;
                break;
                case 3: System.out.println("A área total é: ");System.out.println(Atotal(altura, lado1, lado2));
                n=0;
                break;
                case 4: System.out.println("O volume é: ");System.out.println(volume(altura, lado1, lado2));;
                n=0;
                break;
                case 5:
                    n=2;
                    break;
                default:
                    n = 1;
                break;
    }}}
    public static double Abases(double altura, double lado1, double lado2){
        return 2*(lado1*lado2);
    }
    public static double Alateral(double altura, double lado1, double lado2){
        return 2*(lado1*altura)+2*(lado2*altura) ;
    }
    public static double Atotal(double altura, double lado1, double lado2){
        return (lado1*lado2)*2 + (lado2*altura)*2 + (lado1*altura)*2;
    }
    public static double volume(double altura, double lado1, double lado2){
        return lado1*lado2*altura;
    }

    
}
